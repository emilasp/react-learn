import { createStackNavigator } from 'react-navigation'
import { ROUTE_CAMERA_SCANNER } from '../../constants/routes'
import ScannerScreen from './ScannerScreen'

export default createStackNavigator(
  {
    [ROUTE_CAMERA_SCANNER]: ScannerScreen
  },
  { headerMode: 'none' }
)