import React, { Component } from 'react'
import { connect } from 'react-redux'
import { RNCamera } from 'react-native-camera'

import { View } from 'react-native'
import { searchChanged, getMovies } from '../../actions'
import { Header } from '../../components/uikit'
import { ROUTE_HOME } from '../../constants/routes'

class ScannerScreen extends Component {
    state = {
      title: 'STAR GATE',
      visibleSearchBar: false
    }

    _onChangeText = text => {
      this.props.searchChanged(text)
      this.props.getMovies(text)
    }

    render() {
      const { title, visibleSearchBar } = this.state
      const { navigation, movie, data } = this.props
          //https://www.toptal.com/react-native/react-native-camera-tutorial

      return (
        <View>

          <Header
            title="Scanner"
            colorRight="#fff"
            iconRight="scanner"
            onPressText={() => navigation.navigate(ROUTE_HOME)}
            onPressRight={() => this.setState({ visibleSearchBar: true })}
          />

          {/*<RNCamera
            ref={ref => {
              this.camera = ref
            }}
            style={{
              flex: 1,
              width: '100%'
            }}
          />*/}
        </View>
      )
    }
}

const mapStateToProps = state => ({
  movie: state.search.movie,
  data: state.search.data
})

export default connect(mapStateToProps, { searchChanged, getMovies })(ScannerScreen)
