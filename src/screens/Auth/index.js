import { createStackNavigator } from 'react-navigation'
import { ROUTE_AUTH_LOGIN } from '../../constants/routes'
import LoginScreen from './LoginScreen'

export default createStackNavigator(
  {
    [ROUTE_AUTH_LOGIN]: LoginScreen
  },
  { headerMode: 'none' }
)