import React, { Component } from 'react'
import { connect } from 'react-redux'

import { View, Text, StyleSheet } from 'react-native'
import { Button, Input, Item } from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome'

import { Header } from '../../components/uikit'

import { sendLoginData } from '../../actions'
import { ROUTE_HOME } from '../../constants/routes'
import { w } from '../../../constants'

class LoginScreen extends Component {
    state = {
      login: {
        username: '',
        password: ''
      },
      user: null
    }

    _onChangeLogin = text => {
      this.setState(prevState => ({ login: { ...prevState.login, username: text } }))
    }

    _onChangePassword = text => {
      this.setState(prevState => ({ login: { ...prevState.login, password: text } }))
    }

    _onLogin = () => {
      this.props.sendLoginData(this.state.login.username, this.state.login.password)
        console.log('Login props', this.state, this.props)
    }

    render() {
      const { navigation } = this.props
      const { styleIcon, styleInput } = styles

      return (
        <View>

          <Header
            title="Scanner"
            colorRight="#fff"
            iconRight="scanner"
            onPressText={() => navigation.navigate(ROUTE_HOME)}
          />

          <View>
 <Icon name="user" size={24} color="black" style={styleIcon} />
            <Item rounded>


              <Input
                placeholder="Логин"
                containerStyle={styleInput}
                value={this.state.login.username}
                onChangeText={this._onChangeLogin}
              />
            </Item>

            <Item>

              <Input
                placeholder="Пароль"
                secureTextEntry
                containerStyle={styleInput}
                value={this.state.login.password}
                onChangeText={this._onChangePassword}
              />
            </Item>

            <Button onPress={this._onLogin}><Text>Авторизоваться</Text></Button>

          </View>
        </View>
      )
    }
}


const styles = StyleSheet.create({
  styleIcon: {
    marginRight: 10
  },
  styleInput: {
    width: w - 40,
    marginTop: 10,
    marginBottom: 10
  }
})

const mapStateToProps = state => ({
  user: state.login.user
})

export default connect(mapStateToProps, { sendLoginData })(LoginScreen)
