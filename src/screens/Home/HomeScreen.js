import React, { Component } from 'react'
import { connect } from 'react-redux'
import Config from 'react-native-config'

import { View, Text, TextInput, StyleSheet, TouchableOpacity, Alert } from 'react-native'


import { searchChanged, getMovies } from '../../actions'
import { Layout, Header, HeaderWithSearchBar, ImageCard } from '../../components/uikit'
import { ROUTE_AUTH_LOGIN, ROUTE_CAMERA_SCANNER, ROUTE_HOME_DETAILS } from '../../constants/routes'
import NotifService from '../../NotifService'

class HomeScreen extends Component {
  state = {
    senderId: '1',
    title: 'STAR GATE',
    visibleSearchBar: false
  }

  constructor(props) {
    super(props)
    this.notif = new NotifService(this.onRegister.bind(this), this.onNotif.bind(this))
  }


    _onChangeText = text => {
      this.props.searchChanged(text)
      this.props.getMovies(text)
    }

    render() {
      const { title, visibleSearchBar } = this.state
      const { navigation, movie, data } = this.props


      console.log('Config', Config)


      return (
        <View>
          { visibleSearchBar ?
            <HeaderWithSearchBar
              colorRight="#fff"
              iconRight="magnify"
              placeholder="search"
              onChangeText={this._onChangeText}
              onPressRight={() => this.setState({ visibleSearchBar: false })}
              onBlur={() => this.setState({ visibleSearchBar: true })}
              value={movie}
            /> :
            <Header
              title={title}
              colorRight="#fff"
              iconRight="magnify"
              onPressText={() => navigation.navigate(ROUTE_AUTH_LOGIN, ({ onGoBack: this.onGoBack }))}
              onPressRight={() => this.setState({ visibleSearchBar: true })}
            />}

          <Layout>

            <View style={styles.container}>
              <Text style={styles.title}>Example app react-native-push-notification</Text>
              <View style={styles.spacer} />
              <TextInput style={styles.textField} value={this.state.registerToken} placeholder="Register token" />
              <View style={styles.spacer} />

              <TouchableOpacity style={styles.button} onPress={() => { this.notif.localNotif() }}><Text>Local Notification (now)</Text></TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => { this.notif.scheduleNotif() }}><Text>Schedule Notification in 30s</Text></TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => { this.notif.cancelNotif() }}><Text>Cancel last notification (if any)</Text></TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => { this.notif.cancelAll() }}><Text>Cancel all notifications</Text></TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => { this.notif.checkPermission(this.handlePerm.bind(this)) }}><Text>Check Permission</Text></TouchableOpacity>

              <View style={styles.spacer} />
              <TextInput style={styles.textField} value={this.state.senderId} onChangeText={(e) => { this.setState({ senderId: e }) }} placeholder="GCM ID" />
              <TouchableOpacity style={styles.button} onPress={() => { this.notif.configure(this.onRegister.bind(this), this.onNotif.bind(this), this.state.senderId) }}><Text>Configure Sender ID</Text></TouchableOpacity>
              {this.state.gcmRegistered && <Text>GCM Configured !</Text>}

              <View style={styles.spacer} />
            </View>


            <Text>Textsad: {Config.API_URL}</Text>


            {data.map(item => (
              <ImageCard
                data={item.show}
                key={item.show.id}
                onPress={() => navigation.navigate(ROUTE_HOME_DETAILS, ({ show: item.show, onGoBack: this.onGoBack }))}
              />
            ))}
          </Layout>
        </View>
      )
    }

    onRegister(token) {
      Alert.alert('Registered !', JSON.stringify(token))
      console.log(token)
      this.setState({ registerToken: token.token, gcmRegistered: true })
    }

    onNotif(notif) {
      console.log(notif)
      Alert.alert(notif.title, notif.message)
    }

    handlePerm(perms) {
      Alert.alert('Permissions', JSON.stringify(perms))
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%'
  },
  spacer: {
    height: 10
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center'
  }
})

const mapStateToProps = state => ({
  movie: state.search.movie,
  data: state.search.data
})

export default connect(mapStateToProps, { searchChanged, getMovies })(HomeScreen)
