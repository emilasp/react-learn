import React, { PureComponent } from 'react'
import { ScrollView, View, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Header, ImageBigCad } from '../../components/uikit'

class DetailScreen extends PureComponent {
  render() {
    const { onGoBack } = this.props.navigation.state.params
    const { image, name, summary } = this.props.navigation.state.params.show
    const { navigation } = this.props
    const { container, h1, h2, sub } = styles
    const data = { image, name }

    return (
      <View style={container}>
        <Header
          title={name}
          onPressLeft={() => navigation.goBack()}
          iconLeft="ios-arrow-back"
          colorLeft="#fff"
        />

        <ScrollView>
          <View style={sub}>
            <ImageBigCad data={data} />
            <Text style={h1}>{name.toUpperCase()}</Text>
            <Text style={h2}>{summary}</Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white'
  },
  h1: {
    fontSize: 30,
    padding: 15,
    textAlign: 'center'
  },
  h2: {
    fontSize: 15,
    padding: 15,
    textAlign: 'left',
    color: 'grey'
  },
  sub: {
    flex: 1,
    alignItems: 'center',
    marginBottom: 150,
    backgroundColor: 'white'
  }
})

export default DetailScreen