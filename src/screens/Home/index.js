import { createStackNavigator } from 'react-navigation'
import { ROUTE_HOME, ROUTE_HOME_DETAILS, ROUTE_CAMERA_SCANNER, ROUTE_AUTH_LOGIN } from '../../constants/routes'
import HomeScreen from './HomeScreen'
import DetailScreen from './DetailScreen'
import ScannerScreen from '../Camera/ScannerScreen'
import LoginScreen from '../Auth/LoginScreen'

export default createStackNavigator(
  {
    [ROUTE_HOME]: HomeScreen,
    [ROUTE_HOME_DETAILS]: DetailScreen,
    [ROUTE_CAMERA_SCANNER]: ScannerScreen,
    [ROUTE_AUTH_LOGIN]: LoginScreen
  },
  { headerMode: 'none' }
)