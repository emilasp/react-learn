//import
import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { w, BLUE } from '../../../constants'
//body
const Header = ({
  iconLeft,
  iconRight,
  colorLeft,
  colorRight,
  title,
  onPressLeft,
  onPressRight,
  onPressText
}) => {
  const { container, textStyle, iconStyleLeft, iconStyleRight } = styles

  return (
    <View style={container}>
      {iconLeft &&
        <TouchableOpacity onPress={onPressLeft}>
          <Ionicons name={iconLeft} style={iconStyleLeft} color={colorLeft} />
        </TouchableOpacity>}

      <Text
          numberOfLines={1}
          ellipsezeMode="tail"
          style={textStyle}
          onPress={onPressText}
      >{title}</Text>

      {iconRight &&
      <TouchableOpacity onPress={onPressRight}>
        <MaterialCommunityIcons name={iconRight} style={[iconStyleRight, { color: colorRight }]} />
      </TouchableOpacity>}

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    paddingHorizontal: 20,
    backgroundColor: BLUE,
    position: 'relative',
    height: 90
  },
  textStyle: {
    color: '#fff',
    fontSize: 28,
    width: w - 75,
    paddingTop: 40
  },
  iconStyleLeft: {
    paddingTop: 40,
    fontSize: 35
  },
  iconStyleRight: {
    paddingTop: 44,
    fontSize: 30,
    marginRight: 3
  }
})


//export
export { Header }