import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { w, h } from '../../../constants'


const ImageCard = ({ data, onPress }) => {
  const { container, sub, title, cover } = styles
  const { image, name } = data
  const img = image === null ? 'http://fcrmedia.ie/wp-content/themes/fcr/assets/images/default.jpg' : image.medium

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={container}>
        <View style={sub}>
          <Image style={cover} source={{ uri: img }} />
        </View>

        <Text style={title}>{name.toUpperCase()}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    width: w / 2.4,
    paddingVertical: 10
  },
  sub: {
    shadowColor: '#000',
    shadowRadius: 8,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.4,
    borderRadius: 10
  },
  title: {
    fontSize: 18,
    alignSelf: 'center',
    textAlign: 'center',
    width: w / 2.4,
    paddingTop: 10
  },
  cover: {
    width: w / 2.4,
    height: w * 0.63,
    borderRadius: 10,
    backgroundColor: 'white'
  }
})

export { ImageCard }