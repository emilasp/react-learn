//import
import React from 'react'
import { View, TextInput, StyleSheet, TouchableOpacity } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { w, BLUE } from '../../../constants'
//body
const HeaderWithSearchBar = ({
  iconRight,
  colorRight,
  onPressRight,
  onChangeText,
  onBlur,
  placeholder,
  value
}) => {
  const { container, sub, iconStyleRight, inputStyle, searchStyle } = styles

  return (
    <View style={container}>
      <View style={sub}>
        <TextInput
          style={inputStyle}
          onChangeText={onChangeText}
          onBlur={onBlur}
          placeholder={placeholder}
          value={value}
        />

        {iconRight &&
        <TouchableOpacity onPress={onPressRight}>
          <View style={searchStyle}>
            <MaterialCommunityIcons name={iconRight} style={[iconStyleRight, { color: colorRight }]} />
          </View>
        </TouchableOpacity>}

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    paddingHorizontal: 20,
    backgroundColor: BLUE,
    position: 'relative',
    height: 90
  },
  sub: {
    justifyContent: 'space-between',
    marginTop: 40,
    alignItems: 'center',
    flexDirection: 'row',
    width: w - 35,
    backgroundColor: 'white',
    height: 40,
    borderRadius: 20
  },
  inputStyle: {
    fontSize: 18,
    height: 23,
    width: w - 90,
    marginLeft: 15,
    backgroundColor: 'white'
  },
  searchStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    width: 40,
    height: 40,
    borderRadius: 20

  },
  iconStyleRight: {
    fontSize: 30,
    marginTop: 2
  }
})


//export
export { HeaderWithSearchBar }