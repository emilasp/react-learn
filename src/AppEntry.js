
import App from '../App'

if (__DEV__) {
  activateKeepAwake()
}

registerRootComponent(App)
