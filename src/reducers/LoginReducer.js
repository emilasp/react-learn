import { TYPE_LOGIN_SEND, TYPE_LOGIN_SUCCESS, TYPE_LOGIN_FAILED } from '../constants/types'

const INITIAL_STATE = {
  data: []
}

export default (state = INITIAL_STATE, action) => {
  console.log('Reduser LOGIN', action)
  switch (action.type) {
    case TYPE_LOGIN_SEND:
      return {
        ...state, data: action.payload
      }
    case TYPE_LOGIN_SUCCESS:
      return {
        ...state, user: action.payload
      }
    case TYPE_LOGIN_FAILED:
      return {...state}
    default: return state
  }
}