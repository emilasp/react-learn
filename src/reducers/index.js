import { combineReducers } from 'redux'
import SearchReducer from './SearchReducer'
import LoginReducer from "./LoginReducer";

export default combineReducers({
    search: SearchReducer,
    login: LoginReducer
})