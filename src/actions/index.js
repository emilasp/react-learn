import { MOVIES_FAILED, MOVIES_FETCHED, SEARCH_CHANGE } from '../constants/types'


//Auth
import { TYPE_LOGIN_SEND, TYPE_LOGIN_SUCCESS, TYPE_LOGIN_FAILED } from '../constants/types'
import { LOGIN_URL } from '../constants/fetchUrls'

export const searchChanged = (text) => {
  console.log('Text in action', text)
  return {
    type: SEARCH_CHANGE,
    payload: text
  }
}

export const getMovies = (text) => async (dispatch) => {
  function onSuccess(success) {
    dispatch({ type: MOVIES_FETCHED, payload: success })
    return success
  }
  function onError(error) {
    dispatch({ type: MOVIES_FAILED, payload: error })
    return error
  }
  try {
    const URL = `https://api.tvmaze.com/search/shows?q=${text}`
    const res = await fetch(URL, { method: 'GET' })
    const success = await res.json()
    return onSuccess(success)
  } catch (error) {
    return onError(error)
  }
}

export const sendLoginData = (login, password) => async (dispatch) => {
  function onSuccess(success) {
    dispatch({ type: TYPE_LOGIN_SUCCESS, payload: success })


    return success
  }
  function onError(error) {
    dispatch({ type: TYPE_LOGIN_FAILED, payload: error })
    return error
  }
  console.log('Send', login, password)
  try {
    /*const res = await fetch(LOGIN_URL, { method: 'GET' })*/
    //const res = `{"status": true, "user": {"username": ${login}}}`
    //const success = await res.json()
    const success = { 'status': true, 'user': { 'username': login } }

    if (!success.status) {
      throw new Error(success.message)
    }

    console.log('success', login, password)

    return onSuccess(success.user)
  } catch (error) {
    console.log('error', error)
    return onError(error)
  }
}
