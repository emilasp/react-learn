import React from 'react'
import { Provider } from 'react-redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createStore, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import reducers from './src/reducers'
import Screen from './src/screens/Home'

import configureStore from './src/configureStore'

const store = configureStore()

const App = () => (
  <Provider store={store}>
    <Screen />
  </Provider>
)

export default App